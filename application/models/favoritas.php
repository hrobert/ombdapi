<?php

class favoritas extends CI_Model{

    public function setFavoritas($datos, $usuario, $titulo_pelicula)
    {
        $this->db->select('id_usuario, titulo_pelicula');
        $this->db->from('favoritas'); 
        $this->db->where('id_usuario', $usuario);
        $this->db->where('titulo_pelicula', $titulo_pelicula);

        if (count($this->db->get()->result())==0) {
            return $this->db->insert('favoritas', $datos);
        }else{
            return false;
        }
       

    }
    
    public function enviarFavoritas ( $usuario)
    {
    	$this->db->select('id_favorita, id_usuario,titulo_pelicula, poster,anio');
        $this->db->from('favoritas');
        $this->db->where('id_usuario', $usuario);
        return $this->db->get()->result();
    	
    }


    public function eliminarFavoritas ($id_favorita)
    {
        
        $this->db->where('id_favorita', $id_favorita);
        return $this->db->delete('favoritas');
        
        
    }
}
    
    ?>