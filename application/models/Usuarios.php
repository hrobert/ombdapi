<?php

class Usuarios extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->db->reconnect();
	}


	/**
	 * Add a new usuario
	 * @param $usuario
	 * @return bool
	 */

    public function setUsuario($datos, $usuario)
    {	
    
    	 $this->db->select('usuario');
    	 $this->db->from('usuarios');
       	 $this->db->where('usuario', $usuario);
       	
     if (count($this->db->get()->result())==0)
      {
     	return $this->db->insert('usuarios', $datos);
     }else
     {
     	return false;
     }
    

   
    	
    }


    public function usuarioUnico($usuario){
        $this->db->select('usuario');
       $consulta= $this->db->where('usuario', $usuario);
       
        if($consulta){
            return true;
        }
        return $this->db->insert('usuarios', $datos);
 
    }

	public function login($usuario, $contrasenia){
		$array = array('usuario'=>$usuario, 'contrasenia'=>$contrasenia);
		$this->db->select('usuario');
		$this->db->from('usuarios');
		$this->db->where($array);
		$resultado = $this->db->get()->result();
		if($resultado){
			return true;
		}
		return false;
    }
	
	public function buscarUsuario($usuario)
	{
		$this->db->select('id_usuario');
		$this->db->from('usuarios');
		$this->db->where('usuario', $usuario);
		return $this->db->get()->row();

	}

}