<?php

class auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios');
    }
    


	public function inicio(){
	session_start();
	if ($_SESSION["token"])
		{
			$this->load->view('inicio');
		}else{
			$this->load->view('welcome_message');
		}
	}

	public function session(){
			$usuario = $this->input->post('usuario');
		$contrasenia = $this->input->post('contrasenia');
        $sesion = $this->Usuarios->login($usuario, $contrasenia);
		if($sesion){
			session_start();
			$_SESSION["token"]=(string)"true";
			$_SESSION["user"]=(string)$usuario;
			header("Location: /prueba/index.php/auth/inicio");
		} else { ?>
			<script>
				alert("Usuario o contraseña incorrecto");
				
			</script>
			<?php $this->load->view('welcome_message');
			header("Location: /prueba");
		 }
	}


	public function logout(){
		session_start();
		session_destroy();
		header("Location: /prueba");

	}

}