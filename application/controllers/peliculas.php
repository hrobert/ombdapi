<?php

class peliculas extends CI_Controller{
        
    public function __construct()
	{
		parent::__construct();
        $this->load->model('Usuarios');
        $this->load->model('favoritas');

    }
    
        public function resultado(){
            session_start();
            $buscar = $_POST["buscar"];
            
        $ruta = 'http://www.omdbapi.com/?i=tt3896198&apikey=4f8b3d5f&t='.$buscar;
        $curl = curl_init($ruta);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
        $respuesta = curl_exec($curl);
        curl_close($curl);
         $usuarios=$this->Usuarios->buscarUsuario($_SESSION["user"]);
        $json = json_decode($respuesta);
        $data['respuesta'] = $json;
        $data['title'] = 'Peliculas';
        $data['usuario']= $usuarios->id_usuario;
        $this->load->view('peliculas', $data);

        }

        public function guardarFavorito(){
           session_start();
            $usuarios=$this->Usuarios->buscarUsuario($_SESSION["user"]);
            if ($this->input->post())
        {
            if ( $this->favoritas->setFavoritas($this->input->post(),$usuarios->id_usuario, $this->input->post('titulo_pelicula'))) {
                 header("Location: /prueba/index.php/peliculas/buscarFavorito");
            }
                else{  ?>
                    <script>
                     alert("Pelicula Existente");
                    </script>
                    <?php 
                    
                    }



        }
    }


        public function buscarFavorito(){
            session_start();
            $usuarios=$this->Usuarios->buscarUsuario($_SESSION["user"]);
            $favUsuario=$this->favoritas->enviarFavoritas($usuarios->id_usuario);
              $data['favUsuario']= $favUsuario;
            $this->load->view('favoritas', $data);


        }

        public function eliminarFavorito($id_favorita){

            if ($this->favoritas->eliminarFavoritas($id_favorita))
                { ?>
                    <script>
                alert("Pelicula Eliminada de favoritos Correctamente");
                
            </script>
            <?php 
            header("Location: /prueba/index.php/peliculas/buscarFavorito");
                } else{
                    ?>
                    <script>
                alert("Error al eliminar pelicula de favoritas");
                
            </script>

            <?php 
        
                }
                    

        }
                
    }
    
