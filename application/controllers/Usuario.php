<?php

class Usuario extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios');
	}

    public function index()
    {
        $usuarios=$this->Usuarios->getUsuarios();
        $this->load->view("usuarios", compact('usuarios'));
    }

    
    //guardar
    public function guardar()
    {
        if ($this->input->post())
        {
            if ( $this->Usuarios->setUsuario($this->input->post(), $this->input->post('usuario'))) {
                 ?>
                    <script>
                     alert("Usuario Registrado");
                       history.back();
                       </script>
                       <?php 
            }
                else{  ?>
                    <script>
                     alert("Usuario existente");
                       history.back();
                    </script>
                    <?php 

                    }
        
        
        }
    }

}