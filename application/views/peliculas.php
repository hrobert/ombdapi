<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php

include 'session.php';
include 'head.php';
?>
<title>
	peliculas
</title>

<body>
<div class="container border bg-dark mt-4" >
<H2 class="h1 text-center mt-4 text-white">Resultado</H2>
<hr class="bg-danger">
</div>
<div class="container mt-3">
    <div class="col d-flex justify-content-center">
<div class="card" style="width: 18rem;">
<form action="../peliculas/guardarFavorito" method="post" >
  <img src="<?php echo $respuesta->Poster; ?>" class="card-img-top" alt="...">
  <div class="card-body bg-dark text-white">
    <h5 class="card-title"><?php echo $respuesta->Title; ?></h5>
    <p class="card-text"> <?php echo $respuesta->Year; ?></p>

    <input type="text" name="id_usuario" hidden="" value="<?php echo $usuario; ?>">
    <input type="text" name="poster" hidden="" value="<?php echo $respuesta->Poster; ?>">
    <input type="number" name="anio" hidden="" value="<?php echo $respuesta->Year; ?>">
    <input type="text" name="titulo_pelicula" hidden="" value="<?php echo $respuesta->Title; ?>">
    
    <button type="submit" class="btn btn-warning">Favorito</button>
  </div>
  </form>
</div>
</div>
</div>



</body>