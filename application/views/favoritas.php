
<!DOCTYPE html>

<html>
<head>

	<?php
include 'session.php';
include 'head.php'; 
?>

	<title>Favoritas</title>
</head>
<body>
	<div class="container border bg-dark mt-4" >
<H2 class="h1 text-center mt-4 text-white">Favoritas</H2>
<hr class="bg-danger">
</div>

	<?php foreach($favUsuario as $pelicula){?>


<div class="container mt-3">
    <div class="col d-flex justify-content-center ">
<div class="card" style="width: 18rem;">
  <img src="<?php echo $pelicula->poster; ?>" class="card-img-top" alt="...">
  <div class="card-body bg-dark text-white">
    <h5 class="card-title"><?php echo $pelicula->titulo_pelicula; ?></h5>
    <p class="card-text"> <?php echo $pelicula->anio; ?></p>
    <a type="button" href="../peliculas/eliminarFavorito/<?php echo $pelicula->id_favorita; ?>" class="btn btn-danger">Eliminar de Favoritas</a>
      </div>

</div>
</div>
</div>

<?php } ?> 
</body>
</html>