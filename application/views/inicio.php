<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php

include 'session.php';
include 'head.php';
?>

<title>
  Inicio
</title>
<body>
<div class="container border bg-dark mt-4" >
<H2 class="h1 text-center mt-4 text-white">BIENVENIDO AL SITIO DE PELICULAS</H2>
<hr class="bg-danger">
</div>

<div class="container">
<h4 class="h4 text-center mt-3">¿Qué desea hacer?</h4>

<div class="row ">
<div class="col  text-center mt-3">
<a class="btn btn-primary" href="../peliculas/buscarFavorito" type="button">Peliculas Favoritas</a>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Buscar Nueva Pelicula
  </button>

 <!--inicia callapse buscar nueva pelicula-->
  <div class="collapse mt-4" id="collapseExample">
  <div class="card card-body">

  <!--inicia buscador-->
  <div class="row">
  <div class="col d-flex justify-content-center">

  <form class="form-inline my-2 my-lg-0" action="../peliculas/resultado" method="post">
      <input class="form-control mr-sm-2" type="text" placeholder="Buscar"  name="buscar" id="buscar" aria-label="Search">
      <button class="btn bg-danger text-white my-2 my-sm-0" type="submit">Buscar</button>
    </form>

    </div>
    </div>
 <!--finaliza buscador-->

  </div>
  </div>
 <!--finaliza collapse buscar nueva pelicula-->



</div>
</div>

</div>


</body> 